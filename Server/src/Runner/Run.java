package Runner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.util.Scanner;

import Model.model.SolutionManager;
import Network.MyTCPIPServer;

/**
 * public class Run 
 * run class
 */
public class Run {

	private MyTCPIPServer server;

	/**
	 * public static void main(String[] args)
	 * main
	 */
	public static void main(String[] args) {
		Run run = new Run();
		System.out.println("Server side");
		String action = "";
		BufferedReader reader=null;
		reader= new BufferedReader(new InputStreamReader(System.in));
		do {
			System.out.print("Enter command: ");
			try {
				action = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			run.handleAction(action);
		} while (!(action.equals("exit")));//read command until exit
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();}
	}

	/**
	 * 	private void handleAction(String action)
	 * manage action according command
	 */
	private void handleAction(String action) {
		String sp[] = action.split(" ");
		if (sp[0].equals("start")) {
			server = MyTCPIPServer.getInstance();//create server
			server.startServer();//start server activity
		} else if (sp[0].equals("exit")) {
			server.stopServer();//stop server activity
			SolutionManager.getInstance().saveSolutionsInFile();
		} else {
			System.out.println("Invalid command. Try again");//if not enter start/exit
		}
	}
}
