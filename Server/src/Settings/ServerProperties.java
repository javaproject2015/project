package Settings;

import java.io.Serializable;

//Class that contains the server settings 
public class ServerProperties implements Serializable {

	//data members
	private static final long serialVersionUID = 1L;
	//port server listing
	private int port;
	//number of  server's clients
	private int numOfClients;
	
	//c'tor
	public ServerProperties(int port, int numOfClients) {
		super();
		this.port = port;
		this.numOfClients = numOfClients;
	}	
	//default c'tor
	public ServerProperties() { }

	//set&get
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public int getNumOfClients() {
		return numOfClients;
	}
	
	public void setNumOfClients(int numOfClients) {
		this.numOfClients = numOfClients;
	}
	
}

