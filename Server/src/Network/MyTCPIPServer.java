package Network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import Settings.HandleProperties;
import Settings.ServerProperties;
import Tasks.TaskRunnable;


/**
 * public class MyTCPIPServer
 *server class
 */
public class MyTCPIPServer {

	// data members
	// server socket
	private ServerSocket server;
	// client socket
	private Socket socket;
	// thread's pool
	private ExecutorService executor;
	// port number
	private int port;
	// clients number
	private int numOfClients;
	// boolean member to stop server
	private boolean stop;
	// array list of client
	private ArrayList<ClientHandler> clients;
	// object of server
	private static MyTCPIPServer instance;
	/**
	 * public MyTCPIPServer()
	 *server c'tor- read parameters from xml file
	 */
	public MyTCPIPServer() {
		ServerProperties properties = HandleProperties.readProperties();// read from xml file
		this.stop = true;
		this.socket = null;
		this.port = properties.getPort();
		this.numOfClients = properties.getNumOfClients();
		this.clients = new ArrayList<ClientHandler>();
	}
	/**
	 * public static MyTCPIPServer getInstance()
	 * create server only once
	 */
	public static MyTCPIPServer getInstance() {
		if (instance == null) {
			instance = new MyTCPIPServer();
		}
		return instance;
	}
	/**
	 * public void startServer()
	 *start server activity
	 */
	public void startServer() {
		try {
			server = new ServerSocket(port);// open server socket
			server.setSoTimeout(100000);// set time out
			executor = Executors.newFixedThreadPool(numOfClients);// create thread's pool of clients thread's pool of clients
			Thread thread = new Thread(new Runnable() {

				@Override
				public void run() {
					do {
						try {
							socket = server.accept();// listing to port
							if (stop && socket != null) {
								ClientHandler handler = new ClientHandler(socket);// crate clientHandler
								clients.add(handler);// add client to array list of clients
								executor.submit(new TaskRunnable(handler));
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							System.out.println("finish time out!");
						} catch (Exception e) {
						}
					} while (stop && ((socket == null) || !(socket.isClosed())));
					try {
						executor.shutdown();// close the server
						server.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			thread.start();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	/**
	 * public void stopServer()
	 *stops the server activity in an organized way
	 */
	public void stopServer() {
		stop = false;
		if (clients != null) {
			for (int i = 0; i < clients.size(); i++)
				clients.get(i).closeClient();
		}

	}

}