package Network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;

import Model.model.Model;
import Model.model.MyModel;
import Model.model.Problem;
import Model.model.Solution;
import Model.model.SolutionManager;
import Tasks.Task;

/**
 * public class ClientHandler implements Task
 *class that defines  client in server-side 
 */
public class ClientHandler implements Task {

	// client socket
	private Socket socket;

	// client commands
	private HashMap<String, Command> commands;
	// read from client
	private ObjectInputStream in;
	// write to client

	private ObjectOutputStream out;
	// boolean member to stop client
	private boolean stop;

	Model model;
	// command interface
	public interface Command {
		void doCommand();
	}
	/**
	 * public ClientHandler(Socket socket)
	 * c'tor
	 */
	public ClientHandler(Socket socket) {
		this.stop = true;
		this.socket = socket;
		this.model=null;
		this.commands = new HashMap<String, Command>();
		// initialize the HashMap
		commands.put("SolveDomain", new SolveDomain());
		commands.put("ShowSolution", new ShowSolution());
		commands.put("exit", new exitCommand());
		try {
			this.out = new ObjectOutputStream(socket.getOutputStream());
			this.in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * public void doTask()
	 * do task method that manage client commands
	 */
	// do task method that manage client commands
	@Override
	public void doTask() {

		try {
			String str="";
			while (stop && (str = (String) in.readObject()) != null){
				commands.get(str).doCommand(); }
			} catch (IOException e) {
				if(!socket.isClosed())
					closeClient(); 
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	/**
	 * 	private class SolveDomain implements Command
	 *  class that solve domain
	 */
	// class that solve domain
	private class SolveDomain implements Command {
		/* (non-Javadoc)
		 * @see Network.ClientHandler.Command#doCommand()
		 */
		/* (non-Javadoc)
		 * @see Network.ClientHandler.Command#doCommand()
		 */
		/* (non-Javadoc)
		 * @see Network.ClientHandler.Command#doCommand()
		 */
		@Override
		public void doCommand() {
			Problem problem = null;
			try {
				problem = (Problem) in.readObject();
				model = new MyModel();// create model and insert data
				model.selectDomain(problem.getDomainDetails());
				model.selectAlgorithm(problem.getAlgorithmName());
				model.solveDomain();
				try{
					if(model.getDomain().getDescription()!=null)
						out.writeObject((String)model.getDomain().getDescription());
					/*if(!socket.isClosed())
						out.writeObject((Solution)model.getSolution());
				closeClient();*/
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("client not found!");
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	private class ShowSolution implements Command {

		@Override
		public void doCommand() {
			try{
				if(!socket.isClosed()){
					out.writeObject((Solution)model.getSolution());}
				closeClient();
				} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	/**
	 *private class exitCommand implements Command
	 * class that close the connection with the client
	 */
	private class exitCommand implements Command {

		@Override
		public void doCommand() {
			stop = false;
			SolutionManager.getInstance().saveSolutionsInFile();// save
																// solutions in
																// file
	
			try {
				if (in != null)
					in.close();// close write option
				if (out != null)
					out.close();// close read option
				if (socket != null)
					socket.close();// close socket
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	/**
	 * public void closeClient() 
	 * method that server use when need to stops activity in an organized way
	 */
	public void closeClient() {
		stop = false;
		SolutionManager.getInstance().saveSolutionsInFile();// save solutions in
															// file
		if (in != null)
			try {
				in.close();// close write option
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if (out != null)
			try {
				out.close();// close read option
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if (socket != null)
			try {
				socket.close();// close socket
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
