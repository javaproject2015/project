package Model.Algorithm;

import java.util.HashMap;

/**
 * public interface SearchDomain 
 * interface that contains the relevant methods is in common to all search problems
 */
public interface SearchDomain 
{
	/**
	 * State getStartState()
	 * method that return the start state
	 */
	State getStartState();
	/**
	 * 	State getGoalState() 
	 * method that return the goal state
	 */
	State getGoalState();
	/**
	 * HashMap<Action,State> getAllPossibleMoves(State current)
	 * method that return HashMap with all possible moves
	 */
	HashMap<Action,State> getAllPossibleMoves(State current);	
	/**
	 * String getProblemDescription()
	 * method that return string with all the data about the problem
	 */
	String getProblemDescription();	
	
	String getDescription();
	
}
