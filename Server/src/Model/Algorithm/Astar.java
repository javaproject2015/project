package Model.Algorithm;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * public class Astar extends HeuristicSearcher
 *class of A-STAR algorithm
 */
public class Astar extends HeuristicSearcher {
	
	// data member
	
	// HashMap that saves for each state its g_score (the distance from the start state)
	protected HashMap<State, Double> Gbysta = new HashMap<State, Double>();

	/**
	 * public ArrayList<Action> search(SearchDomain domain)
	 * method that Includes all steps under the algorithm A-STAR
	 */
	@Override
	public ArrayList<Action> search(SearchDomain domain) {
		// check if the class get heuristic function
		if (h != null) {
			// Variable that save the state's g_price
			double g_price = 0;
			// Variable for the start state
			State stateStart = domain.getStartState();
			// Variable for the goal state
			State stateGoal = domain.getGoalState();
			// initialization start state data: price,came from and action.
			stateStart.setPrice(h.HeuristicFunc(stateStart, stateGoal));
			stateStart.setCameFrom(null);
			stateStart.setCameWithAction(null);
			// add start state to the open list
			openList.add(stateStart);
			// put the s_price of the start state in HashMap
			Gbysta.put(stateStart, g_price);

			// Pass on the priority queue until it is empty(at first include only the start)
			while (!openList.isEmpty() && !stop) { 
				// remove a state from the queue
				State current = openList.poll();
				// check if we reached the goal state, and return the path
				if (current.equals(domain.getGoalState()))
					return reconstruct_path(current);
				// add the state we finish deal with to the close queue
				closedList.add(current);
				// HashMap that saves for each state its all possible moves
				HashMap<Action, State> SbyAct = domain
						.getAllPossibleMoves(current);
				// pass on all action in the HashMap
				for (Action act : SbyAct.keySet()) {
					// get the neighbor state according to the action
					State state = SbyAct.get(act);
					// check if we deal with this state
					if (closedList.contains(state))
						continue;
					// calculate the g_price of the state
					g_price = Gbysta.get(current) + act.getPrice();
					// check if we found a better way to reach the state
					if (!openList.contains(state) || g_price < state.getPrice()) {
						// update state data: the parent and action
						state.setCameFrom(current);
						state.setCameWithAction(act);
						// put the g_price we calculate into the HashMap
						Gbysta.put(state, g_price);
						// update state price: price=g_price+heuristic value
						state.setPrice(Gbysta.get(state)
								+ h.HeuristicFunc(state, stateGoal));
						// add the neighbor state to the open list if not exist
						if (!openList.contains(state))
							openList.add(state);
					}
				}
			}
		}
		// return null if not found a path or if have a problem
		return null;
	}
}
