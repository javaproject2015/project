package Model.Algorithm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import org.junit.Test;
import Model.Domains.MazeGameDomain;

/**
 *public class BFSTest
 * class that checking the correctness of BFS algorithm
 */
public class BFSTest {
	/**
	 *public void test()
	 * Test
	 */
	@Test
	public void test() {
			
		SearchDomain domain= new MazeGameDomain(3,3,0);
		Searcher s =new BFS();
		ArrayList<Action> actions=s.search(domain);
		assertEquals(4,actions.size());
		assertTrue(actions.get(0).toString().equals("right"));
		assertTrue(actions.get(1).toString().equals("down"));
		assertTrue(actions.get(2).toString().equals("right"));
		assertTrue(actions.get(3).toString().equals("down"));
		
		
		
	}

}
