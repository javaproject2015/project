package Model.Algorithm;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *public class BFS extends AbsSearcher
 *
 */
public class BFS extends AbsSearcher {

	@Override
	public ArrayList<Action> search(SearchDomain domain) {
		// add the start state to the open list
		this.openList.add(domain.getStartState());
		// Pass on the priority queue until it is empty(at first include only the start)
		while (!openList.isEmpty() && !stop) {
			// remove a state from the queue
			State state = openList.poll();
			// add the state to the close list
			closedList.add(state);
			// check if we reached the goal state, and return the path
			if (state.equals(domain.getGoalState())) {
				ArrayList<Action> actions = reconstruct_path(state);
				return actions;
			}
			// HashMap that saves all the possible moves for each state
			HashMap<Action, State> nextStates = domain.getAllPossibleMoves(state);
			// pass on all action in the HashMap
			for (Action a : nextStates.keySet()) {
				// get the neighbor state according to the action
				State nextState = nextStates.get(a);
				// calculate the neighbor's price: parent's price + the action between the two
				double newPathPrice = state.getPrice() + a.getPrice();
				// check that the neighbor state didn't treat
				if (!openList.contains(nextState)&& !closedList.contains(nextState)) {
					// update neighbor's data: parent,action and price
					nextState.setCameFrom(state);
					nextState.setCameWithAction(a);
					nextState.setPrice(newPathPrice);
					openList.add(nextState);
				} else { // check if we found better way to reach the state
					if (newPathPrice < nextState.getPrice()) {
						// add the neighbor state to the open list if not exist
						if (!openList.contains(nextState))
							openList.add(nextState);
						else {
							// //update neighbor's data: parent,action and price
							openList.remove(nextState);
							nextState.setCameFrom(state);
							nextState.setCameWithAction(a);
							nextState.setPrice(newPathPrice);
							openList.add(nextState);
						}
					}
				}

			}
		}
		return null;
	}
}
