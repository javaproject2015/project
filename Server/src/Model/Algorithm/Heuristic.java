package Model.Algorithm;
/**
 * public interface Heuristic
 * interface for heuristic function
 */
public interface Heuristic {
	
	public double HeuristicFunc(State s1, State s2);
	
}
