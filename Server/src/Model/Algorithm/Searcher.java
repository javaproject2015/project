package Model.Algorithm;

import java.util.ArrayList;


/**
 * public interface Searcher
 * interface to the all the searchers
 */
public interface Searcher 
{
	/**
	 * public ArrayList<Action> search(SearchDomain domain)
	 *method that search solution to the problems and return list of actions
	 */
	public ArrayList<Action> search(SearchDomain domain);
	/**
	 * public void setStop()
	 *set method of stop
	 */
	public void setStop();
}
