package Model.Algorithm;

/**
 * public abstract class CommonState extends State
 * Class that Extends State class and contains data for games that use a board that requires locations coordinates
 */
public abstract class CommonState extends State{
	
	
	/**
	 *public CommonState(String state)
	 * c'tor
	 */
	public CommonState(String state) {
		super(state);
	}
	/**
	 *public int getX()
	 *method get x-return the row of the state-converts from string to int
	 */
	public int getX() {
		String delimiter = ",";
		String[] temp;
		temp = (this.getState()).split(delimiter);
		int x=Integer.parseInt(temp[0]);
		return x;
	}
	/**
	 * public int getY()
	 * method get y-return the column of the state-converts from string to int
	 */
	public int getY() {
		String delimiter = ",";
		String[] temp;
		temp = (this.getState()).split(delimiter);
		int y=Integer.parseInt(temp[1]);
		return y;
	}
	
	
	
}
