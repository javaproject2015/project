package Model.Algorithm;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * public abstract class AbsSearcher implements Searcher 
 * abstract class that contains all the data common to all searchers
 */
public abstract class AbsSearcher implements Searcher {

	//data members
	//Priority Queue of states we need to deal with them 
	protected PriorityQueue<State> openList = new PriorityQueue<State>();
	//boolean-if switch to true is man that want stop the thread
	boolean stop;
	
	/**
	 * public AbsSearcher()
	 * c'tor-initialize stop to false
	 */
	public AbsSearcher() {
		this.stop = false;
	}
	//Priority Queue of states we finish to deal with them 
	protected PriorityQueue<State> closedList = new PriorityQueue<State>();
	/**
	 *  protected ArrayList<Action> reconstruct_path(State current)
	 * method that return Array List of all the action we did to reach the goal
	 */
	 protected ArrayList<Action> reconstruct_path(State current)
	{
		ArrayList<Action> actions = new ArrayList<Action>();	
		while (current.getCameFrom() != null) 
		{
			 actions.add(0, current.getCameWithAction());
			 current = current.getCameFrom();
		}
				 
	  return actions;
	 }
	 
	 /**
	  *  public void setStop()
	  * set boolean member-stop
	  */
	 public void setStop()
	 {
		 this.stop=true;
	 }
}
