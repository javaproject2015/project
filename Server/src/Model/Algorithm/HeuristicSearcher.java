package Model.Algorithm;

import java.util.ArrayList;


/**
 * public abstract class HeuristicSearcher extends AbsSearcher
 * father class for all searchers who use heuristics
 */
public abstract class HeuristicSearcher extends AbsSearcher {
	
	//data member
	//member from heuristic type-get his value from the domain
	protected Heuristic h = null;
	/**
	 *public  abstract ArrayList<Action> search(SearchDomain domain)
	 *method that each searcher need to implement by his algorithm
	 */
	@Override
	public  abstract ArrayList<Action> search(SearchDomain domain);
	/**
	 * 	public  void SetH (Heuristic h)
	 * set heuristic function
	 */
	//set heuristic function
	public  void SetH (Heuristic h) {
		this.h = h;
	}
	
}
