package Model.Algorithm;
/**
 * public abstract class State implements Comparable<State> 
 * state class describes a general situation for all games
 */
public abstract class State implements Comparable<State> 
{
	//data members
	
	// the string representation of the state
	protected String state;
	// the price of the state
	protected double price;
	// the previous state
	/**
	 * 
	 */
	protected State cameFrom;
	// the action to reach the state
	protected Action cameWithAction;
	/**
	 * public State(String state) 
	 * c'tor
	 */
	public State(String state) 
	{
		this.state=state;
	}
	/**
	 * 	public State getCameFrom() 
	 * get the previous state
	 */
	public State getCameFrom() 
	{
		return cameFrom;
	}
	/**
	 * public void setCameFrom(State cameFrom)
	 * set the previous state
	 */
	public void setCameFrom(State cameFrom) 
	{
		this.cameFrom = cameFrom;
	}
	/**
	 * public double getPrice() 
	 * get state price
	 */
	public double getPrice() 
	{
		return price;
	}
	/**
	 *public void setPrice(double price) 
	 * set state price
	 */
	public void setPrice(double price) 
	{
		this.price = price;
	}
	/**
	 *public String getState()
	 * get the state 
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * public Action getCameWithAction()
	 * get the action to the state
	 */
	public Action getCameWithAction() 
	{
		return cameWithAction;
	}
	/**
	 * public void setCameWithAction(Action cameWithAction) 
	 * set the action to the state
	 */
	public void setCameWithAction(Action cameWithAction) 
	{
		this.cameWithAction = cameWithAction;
	}
	/**
	 * public String toString()
	 * to string of the description	
	 */
	@Override
	public String toString()
	{
			return state;
	}
	/**
	 * public boolean equals(Object o) 
	 * check if two state are the same
	 */
	@Override
	public boolean equals(Object o)
	{
		return state.equals(((State)o).getState());
	}
	/**
	 * public int compareTo(State state)
	 * compare between two state for the priority queue
	 */
	@Override
	public int compareTo(State state)
	{
		if (this.getPrice() > state.getPrice())
			return 1;
		else if (this.getPrice() < state.getPrice())
				return -1;
		return 0;
	}
	/**
	 * public int hashCode()
	 * hash code for the hash map
	 */
	@Override
	public int hashCode() 
	{
		return state.hashCode();
	}
			
		
}


