package Model.Domains;

import Model.Algorithm.CommonState;
import Model.Algorithm.Heuristic;
import Model.Algorithm.State;


/**
 * public class MazeGameHeuristic implements Heuristic
 * class that include the heuristic function of the maze game
 */
public class MazeGameHeuristic implements Heuristic{

	
/**
 * public double HeuristicFunc(State s1, State s2) 
 * calculate aerial distance 
 */
	@Override
	public double HeuristicFunc(State s1, State s2) {
		int x = Math.abs(((CommonState)(s1)).getX() - ((CommonState)(s2)).getX());
		int y = Math.abs(((CommonState)(s1)).getY() - ((CommonState)(s2)).getY());
		double d = Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
		return d;
	}

}
