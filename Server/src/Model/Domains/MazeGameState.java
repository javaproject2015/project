package Model.Domains;

import Model.Algorithm.CommonState;


/**
 * public class MazeGameState extends CommonState 
 * class maze state that extends common state
 */
public class MazeGameState extends CommonState 
{
	/**
	 * protected MazeGameState(String state) 
	 * c`tor
	 */
	protected MazeGameState(String state) 
	{
		super(state);
	}
	

}
