package Model.Domains;

import java.util.HashMap;
import java.util.Random;

import Model.Algorithm.Action;
import Model.Algorithm.SearchDomain;
import Model.Algorithm.State;


/**
 * public class MazeGameDomain implements SearchDomain 
 *class of maze game 
 */
public class MazeGameDomain implements SearchDomain 
{
	//data members
	
	//start maze
	protected MazeGameState mazeStart;
	//goal maze
	protected MazeGameState mazeGoal;
	//board game of integer
	protected int [][] boardGame;
	// row number
	private int row;
	//column number
	private int column;
	// wall number
	private int block;
	// action array- we have 4 action: up, down, left, right
	private Action [] actions=new Action [4];
	/**
	 * public MazeGameDomain (int r,int c, int b) 
	 *c'tor
	 */
	public MazeGameDomain (int r,int c, int b)
	{
		this.row=r;
		this.column=c;
		//check if have more wall then space
		if(b<(r*c)-2)
			this.block=b;
		else
			this.block=(r*c)-2;
		this.mazeStart=new MazeGameState (0+","+0);
		this.mazeGoal=new MazeGameState ((row-1)+","+(column-1));
		boardGame=new int [row][column];
		actions [0]=new Action ("up");
		actions [1]=new Action ("down");
		actions [2]=new Action ("right");
		actions [3]=new Action ("left");
		for(int i=0; i<4; i++)
			actions[i].setPrice(1);
		//Initializes the board game with 0
		initMaze();
		//Sets the walls of the maze
		randomWall(block);
	}
	/**
	 * private void initMaze()
	 * Initializes the board game with 0
	 */
	private void initMaze()
	{
		for (int i=0; i<row; i++)
			for(int j=0;j<column; j++)
			{
				boardGame[i][j]=0;
			}	
	}
	/**
	 * private void randomWall (int numOfwall)
	 * Sets the walls of the maze in random places
	 */
	private void randomWall (int numOfwall)
	{
		Random rand=new Random();
		int r,c, sum=0;
		while (sum<numOfwall)
		{
			r=rand.nextInt(this.row);
			c=rand.nextInt(this.column);
			if(!((r==0 && c==0)||(r==this.row-1&& c==this.column-1)))
			{	
				if(boardGame[r][c]==0)
				{
					boardGame[r][c]=1;
					sum++;
				}
			}
		}	
	}
	
	
	/*
	//method that print the maze
	private void printMaze()
	{
		for (int i=0; i<row; i++)
		{
			for(int j=0;j<column; j++)
			{
				System.out.print((boardGame[i][j])+",");
			}
			System.out.print("\n");
		}
	}*/
	/**
	 * public State getStartState()
	 * return the start maze
	 */
	@Override
	public State getStartState() {
		return mazeStart;
	}
	/**
	 * public State getGoalState()
	 * return the goal maze
	 */
	@Override
	public State getGoalState() {
		return mazeGoal;
	}
	/**
	 * public HashMap<Action, State> getAllPossibleMoves(State current) 
	 * return all possible moves
	 */
	@Override
	public HashMap<Action, State> getAllPossibleMoves(State current) 
	{
		//get state a form of string and decoded the rows and columns
		String delimiter = ",";
		String[] temp;
		temp = (current.getState()).split(delimiter);
		int x=Integer.parseInt(temp[0]);
		int y=Integer.parseInt(temp[1]);
		//HashMap for all the possible moves
		HashMap<Action, State> moves=new HashMap<>();
		State s;
		//check if the state is a wall-in this situation we can't move
		if(boardGame [x][y]!=0)
			return null;
	
		//check if possible move up, update the data of the next state and put in the HashMap
		if (x-1>=0 && boardGame[x-1][y]==0)
		{
			s=new MazeGameState ((x-1)+","+y);
			actions[0].setPrice(1);
			s.setCameFrom(current);
			s.setCameWithAction(actions[0]);
			moves.put(actions[0], s);
		}
		
		//check if possible move down, update the data of the next state and put in the HashMap
		if (x+1<row && boardGame[x+1][y]==0)
		{
			s=new MazeGameState ((x+1)+","+y);
			actions[1].setPrice(1);
			s.setCameFrom(current);
			s.setCameWithAction(actions[1]);
			moves.put(actions[1], s);
		}
		
		//check if possible move right, update the data of the next state and put in the HashMap
		if (y+1< column && boardGame[x][y+1]==0)
		{
			
			s=new MazeGameState (x+","+(y+1));
			actions[2].setPrice(1);
			s.setCameFrom(current);
			s.setCameWithAction(actions[2]);
			moves.put(actions[2], s);
		}
		
		// check if possible move left, update the data of the next state and put in the HashMap
		if (y-1>=0 && boardGame[x][y-1]==0)
		{
			s=new MazeGameState (x+","+(y-1));
			actions[3].setPrice(1);
			s.setCameFrom(current);
			s.setCameWithAction(actions[3]);
			moves.put(actions[3], s);
		}
	return moves;
	}
	/**
	 * public int getRow() 
	 * get maze row
	 */
	public int getRow() {
		return row;
	}
	/**
	 * public void setRow(int row) 
	 * set maze row
	 */
	public void setRow(int row) {
		this.row = row;
	}
	/**
	 * public int getColumn()
	 * get maze column
	 */
	public int getColumn() {
		return column;
	}
	/**
	 * public void setColumn(int column)
	 * set maze column
	 */
	public void setColumn(int column) {
		this.column = column;
	}
	/**
	 * public int getBlock() 
	 * get maze wall
	 */
	public int getBlock() {
		return block;
	}
	/**
	 *public void setBlock(int block)
	 * set maze wall
	 */
	public void setBlock(int block) {
		this.block = block;
	}
	/**
	 *public String getProblemDescription() 
	 * get Problem Description
	 */
	@Override
	public String getProblemDescription() {
		String description= "Maze"+" "+row+" "+column+" "+block+" ";
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<column;j++)
			{
				description+=boardGame[i][j]+",";
			}
		}
		
		return description;
	}
	public String getDescription() {
		String description=row+" "+column+" ";
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<column;j++)
			{
				description+=boardGame[i][j]+" ";
			}
		}
		
		return description;
	}

}
