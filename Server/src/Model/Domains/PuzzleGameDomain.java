package Model.Domains;

import java.util.HashMap;
import java.util.Random;

import Model.Algorithm.Action;
import Model.Algorithm.SearchDomain;
import Model.Algorithm.State;

/**
 * public class PuzzleGameDomain implements SearchDomain  
 *  class of 8 Puzzle game 
 */
public class PuzzleGameDomain implements SearchDomain  
{
	//data members
	//puzzle start
	protected PuzzleGameState PuzzleStart;
	//puzzle goal
	protected PuzzleGameState PuzzleGoal;
	//board game
	protected int[][] boardGame;
	
	//puzzle row
	private int row;
	//puzzle column
	private int column;
	
	/**
	 * public PuzzleGameDomain()
	 * c'tor
	 */
	//c'tor
	public PuzzleGameDomain()
	{
		this.row=3;
		this.column=3;
		this.boardGame=new int [row][column];
		this.PuzzleStart =new PuzzleGameState(RandomPuzzleStart());
		this.PuzzleGoal = new PuzzleGameState(CreatePuzzleGoal ());
		initBoardGame();
		
	}
	/**
	 * private String RandomPuzzleStart ()
	 *create start board randomly 
	 */
	private String RandomPuzzleStart ()
	{
		char [] charArray=new char[] {'9','9','9','9','9','9','9','9','9'};
		int index=0,counter=0,size=0;
		String start="";
		Random rand=new Random();
		while (size<(this.row*this.column))
		{
			index=rand.nextInt(9);
			if(charArray[index]=='9')
			{
				charArray[index]=(char)('0'+counter);
				counter++;
				size++;
			}
		}
		start=String.valueOf(charArray);
		return start;
	}
	/**
	 * private String CreatePuzzleGoal ()
	 *  Create Puzzle Goal
	 */
	private String CreatePuzzleGoal ()
	{
		char [] charArray=new char[] {'1','2','3','4','5','6','7','8','0'};
		return (String.valueOf(charArray));
	}
	/**
	 *  private void initBoardGame()
	 *  initialize the board game with 0
	 */
	private void initBoardGame()
	{
		for(int i=0; i<row;i++)
			for(int j=0; j<column; j++)
				boardGame[i][j]=-1;
	}
	
	
	/**
	 *  public State getStartState()
	 *  get Start State
	 */
	public State getStartState()
	{
		return this.PuzzleStart;
	}
	/**
	 *  public PuzzleGameState getGoalState()
	 * get goal puzzle
	 */
	public PuzzleGameState getGoalState()
	{
		return this.PuzzleGoal;
	}
	/**
	 * private int [][] puzzleBoard(State state)
	 * convert the string puzzle to matrix
	 */
	private int [][] puzzleBoard(State state)
	{
		char [] str=state.getState().toCharArray();
		int [][]matrix=new int [row][column];
		int k=0;
		int i=0;
		int j=0;
		
		for(i=0; i<row;i++)
			for(j=0; j<column; j++)
			{
				matrix[i][j]=Character.getNumericValue(str[k]);
				k++;
			}
		return matrix;
	}
	/*
	//print the puzzle
	private void printPuzzle(State state)
	{
		int [][]matrix=new int [row][column];
		matrix=puzzleBoard(state);
		int i=0;
		int j=0;
		
		for(i=0; i<row;i++)
		{
			for(j=0; j<column; j++)
			{
				System.out.print(matrix[i][j]+" ");
			}
			System.out.print("\n");
		}
	}
	*/
	
	/**
	 * public String IntToString (int[][]matrix,int r,int c)
	 * method that convert to string from integer
	 */
	public String IntToString (int[][]matrix,int r,int c)
	{
		String s="";
		for(int i=0; i<r; i++)
			for(int j=0; j<c;j++)
				s=s+matrix[i][j];
		return s;

	}
	/**
	 *	private int[] findZero (State current)
	 * find the place of 0 in the current state
	 */
	private int[] findZero (State current)
	{
		int [][]matrix=new int [row][column];
		matrix=puzzleBoard(current);
		int [] index=new int [2];
		int i=0;
		int j=0;
		Boolean flag=true;
		
		for(i=0; i<row && flag;i++)
			for(j=0; j<column && flag; j++)
			{
				if(matrix[i][j]==0)
					flag=false;	
			}
		//the index of the 0 in board game
		i--;
		j--;
		index[0]=i;
		index[1]=j;
		//return array with two value-zero row and zero column
		return index;
	}
	/**
	 *		public HashMap<Action,State> getAllPossibleMoves(State current)
	 * return all possible moves
	 */

	@Override
	public HashMap<Action,State> getAllPossibleMoves(State current)
	{
		//convert the current state to matrix
		int [][] matrix=new int [row][column];
		matrix=puzzleBoard(current);
		//find 0 in the current state 
		int [] zeroIndex=new int[2];
		zeroIndex=findZero (current);
		//Variables
		int i=zeroIndex[0];
		int j=zeroIndex[1];
		int num;
		State s;
		Action a;
		HashMap<Action,State> moves=new HashMap<Action,State>();
		//check if possible move up, update the data of the next state and put in the HashMap
		if(i - 1 >= 0 && j< column)
		{
			num =matrix[i][j];
			matrix[i][j] = matrix[i-1][j];
			matrix[i-1][j] = num; 
			s=new PuzzleGameState(IntToString(matrix,3,3));
			a=new Action("switch "+(matrix[i][j])+" and "+(matrix[i-1][j]));
			a.setPrice(1);
			moves.put(a,s);
			num =matrix[i][j];
			matrix[i][j] = matrix[i-1][j];
			matrix[i-1][j] = num; 
		}
		
		//check if possible move down, update the data of the next state and put in the HashMap
		if(i + 1 < row && j< column )
		{
			num =matrix[i][j];
			matrix[i][j] = matrix[i+1][j];
			matrix[i+1][j] = num; 
			s= new PuzzleGameState(IntToString(matrix,3,3));
			a=new Action("switch "+(matrix[i][j])+" and "+(matrix[i+1][j]));
			a.setPrice(1);
			moves.put(a,s);
			num =matrix[i][j];
			matrix[i][j] = matrix[i+1][j];
			matrix[i+1][j] = num; 
		}
		//check if possible move right, update the data of the next state and put in the HashMap
		if(j + 1 < column && i< row )
		{
			num =matrix[i][j];
			matrix[i][j] = matrix[i][j+1];
			matrix[i][j+1] = num; 
			s= new PuzzleGameState(IntToString(matrix,3,3));
			a=new Action ("switch "+(matrix[i][j])+" and "+(matrix[i][j+1]));
			a.setPrice(1);
			moves.put(a,s);
			num =matrix[i][j];
			matrix[i][j] = matrix[i][j+1];
			matrix[i][j+1] = num; 
		}
		//check if possible move left, update the data of the next state and put in the HashMap
		if(j-1 >= 0 && i< row )
		{
			num =matrix[i][j];
			matrix[i][j] = matrix[i][j-1];
			matrix[i][j-1] = num; 
			s=new PuzzleGameState(IntToString(matrix,3,3));
			a=new Action("switch "+(matrix[i][j])+" and "+(matrix[i][j-1]));
			a.setPrice(1);
			moves.put(a,s);
			num =matrix[i][j];
			matrix[i][j] = matrix[i][j-1];
			matrix[i][j-1] = num; 
		}

		return moves;
	}
	/**
	 *	public String getProblemDescription() 
	 * get Problem Description
	 */
	@Override
	public String getProblemDescription() {
		return "Puzzle " +"start: "+ getStartState() + "goal " + getGoalState(); 
	}
	
	@Override
	public String getDescription() {
		return 3+" "+3+" "+getStartState(); 
	}
	
}



