package Model.Domains;

import Model.Algorithm.Heuristic;
import Model.Algorithm.State;

/**
 *	public class PuzzleGameHeuristic implements Heuristic 
 * class that include the heuristic function of the maze game
 */
public class PuzzleGameHeuristic implements Heuristic 
{
	//heuristic Manhattan
	@Override
	public double HeuristicFunc(State s1, State s2) 
	{
		int d=0;
		char [] str1=s1.getState().toCharArray();
		char [] str2=s2.getState().toCharArray();
		int [][] matrix1=toMatrixArray(str1);
		int [][] matrix2=toMatrixArray(str2);
		int [] index1=new int [2];
		int [] index2=new int [2];
		for(int i=0; i<9;i++)
		{
				index1=findCurrent(matrix1,i);
				index2=findCurrent(matrix2,i);
				d+= Math.abs((index1[0]-index2[0])+(index1[1]-index2[1]));
		}
		return d;
	}
	/**
	 *	private int[][] toMatrixArray(char[] charArray)
	 * covert to matrix
	 */
	private int[][] toMatrixArray(char[] charArray)
	{
		int [][] matrix=new int [3][3];
		int k=0;
		for(int i=0; i<3 && k<charArray.length ;i++)
			for(int j=0; j<3; j++)
			{
				matrix[i][j]=Character.getNumericValue(charArray [k]);
				k++;
			}
				
		return matrix;
	}
	/**
	 *	private int[] findCurrent (int[][] matrix, int num)
	 * method that find the current in the Puzzle
	 */
	private int[] findCurrent (int[][] matrix, int num)
	{
		int [] indexs=new int [2];
		for(int i=0; i<3 ;i++)
			for(int j=0; j<3; j++)
			{
				if (matrix[i][j]==num)
				{
					indexs[0]=i;
					indexs[1]=j;
					return indexs;
				}
			}
		return null;
	}
}
	


