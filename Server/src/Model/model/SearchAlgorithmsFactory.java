package Model.model;

import java.util.HashMap;
import Model.Algorithm.Astar;
import Model.Algorithm.BFS;
import Model.Algorithm.Searcher;


/**
 * public class SearchAlgorithmsFactory
 *algorithm factory
 */
public class SearchAlgorithmsFactory {
	
	//data member
	
	//HashMap that saves for each searcher his creator
	private HashMap<String, AlgorithmCreator> algorithms;
	
	/**
	 * public SearchAlgorithmsFactory()
	 *c'tor
	 */
	public SearchAlgorithmsFactory()
	{
		//initialize the hash map
		algorithms = new HashMap<String, AlgorithmCreator>();
		algorithms.put("BFS", new BFSCreator());
		algorithms.put("Astar", new AstarCreator());
	}
	/**
	 * public Searcher createAlgorithm(String algorithmName)
	 * create the searcher according to the hash map: BFS or Astar
	 */
	public Searcher createAlgorithm(String algorithmName)
	{
		AlgorithmCreator creator = algorithms.get(algorithmName);
		Searcher searcher = null;
		if (creator != null)  {
			searcher = creator.create();			
		}
		
		return searcher;
	}
	/**
	 *private interface AlgorithmCreator
	 *interface for all type of searcher creator
	 */
	private interface AlgorithmCreator
	{
		Searcher create();
	}
	/**
	 *private class BFSCreator implements AlgorithmCreator
	 * class that create BFS searcher
	 */
	private class BFSCreator implements AlgorithmCreator
	{
		@Override
		public Searcher create() {
			return new BFS();
		}		
	}
	/**
	 * private class AstarCreator implements AlgorithmCreator
	 * class that create Astar searcher
	 */
	private class AstarCreator implements AlgorithmCreator
	{
		@Override
		public Searcher create() {
	
			return new Astar();
		}		
	}

}


