package Model.model;

import java.io.Serializable;

/**
 * public class Problem implements Serializable
 *class that description a problem
 */
public class Problem implements Serializable {
	
	//data members
	private static final long serialVersionUID = 1L;
	//domain description
	private String domainDetail;
	//algorithm description

	private String algorithmName;
	/**
	 * public String getDomainDetails()
	 * domain description get
	 */
	public String getDomainDetails() {
		return domainDetail;
	}
	/**
	 * public void setDomainDetails(String str)
	 * domain description set
	 */
	public void setDomainDetails(String str) {
		 this.domainDetail=str;
	}
	/**
	 * public String getAlgorithmName()
	 * get Algorithm description Name get
	 */
	
	public String getAlgorithmName() {
		return algorithmName;
	}
	/**
	 * public String getAlgorithmName()
	 * get Algorithm description Name set
	 */
	
	public void setAlgorithmName(String str) {
		 this.algorithmName=str;
	}


}
