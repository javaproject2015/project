package Model.model;

import Model.Algorithm.SearchDomain;
import Tasks.Task;

/**
 *public interface Model extends Task 
 * interface model that include methods all models need
 */
public interface Model extends Task 
{
	/**
	 * void selectDomain(String domainName)
	 * select domain
	 */
	void selectDomain(String domainName);
	/**
	 * void selectAlgorithm(String algorithmName 
	 * select algorithm
	 */
	void selectAlgorithm(String algorithmName);
	/**
	 * void solveDomain()
	 * solve the game
	 */
	void solveDomain();
	/**
	 * 	Solution getSolution()
	 * get the solution
	 */
	Solution getSolution();
	public SearchDomain getDomain() ;
	/**
	 * public void stopModel () 
	 * method that stop the threat of the model
	 */
	public void stopModel ();
}
