package Model.model;

import java.util.HashMap;
import Model.Algorithm.SearchDomain;
import Model.Domains.MazeGameDomain;
import Model.Domains.PuzzleGameDomain;

/**
 *public class SearchDomainFactory 
 *domain factory
 */
public class SearchDomainFactory 
{
	//data member
	
	//HashMap that saves for each domain his creator
	private HashMap<String, DomainCreator> domain;
	//save the specific domain's data 
	String parameters;
	
	//c'tor
	/**
	 * public SearchDomainFactory()
	 * ctor -initialize the hash map
	 */
	public SearchDomainFactory()
	{
		//initialize the hash map
		domain = new HashMap<String,  DomainCreator>();
		domain.put("Maze", new MazeDomainCreator());
		domain.put("Puzzle", new PuzzleDomainCreator());
	}
	/**
	 * 	public SearchDomain createDomain(String domainName)
	 * create the domain according to the hash map: Maze or 8Puzzel
	 */
	public SearchDomain createDomain(String domainName)
	{
		//made separation between the domain name to the rest of his data
		String[] str = domainName.split(":");
		String nameDomain = str[0];
		for(int i=1;i<str.length;i++)
		{
			if(i!= str.length-1)
				parameters=str[i]+" ";
			else
				parameters=str[i];
		}
		DomainCreator creator = domain.get(nameDomain);
		SearchDomain searcher = null;
		if (creator != null)  
		{
			searcher = creator.create();		
		}	
		return searcher;
	}
	/**
	 * private interface DomainCreator
	 * interface for all type of domain
	 */
	private interface DomainCreator
	{
		SearchDomain create();
	}
	/**
	 * private class MazeDomainCreator implements DomainCreator
	 * class that create Maze domain
	 */
	private class MazeDomainCreator implements DomainCreator
	{
		//data members
		private int row=0; 
		private int column=0;
		private int block=0;
		
		@Override
		public SearchDomain create() {
			//made separation between the row,column and the wall
			String[] str = parameters.split(",");
			row=Integer.parseInt(str[0]);
			column=Integer.parseInt(str[1]);
			block=Integer.parseInt(str[2]);
			return new MazeGameDomain(row, column, block);
		}		
	}
	/**
	 *  private class PuzzleDomainCreator implements DomainCreator
	 *  class that create Puzzle domain
	 */
	private class PuzzleDomainCreator implements DomainCreator
	{
		@Override
		public SearchDomain create() {
			// TODO Auto-generated method stub
			return new PuzzleGameDomain();
		}		
	}
}

