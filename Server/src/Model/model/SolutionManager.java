package Model.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;


/**
 * public class SolutionManager 
 *class that manage all the solution
 */
public class SolutionManager {
	
	//data members
	//hash map that save for all description his solution
	private HashMap<String, Solution> solutionsMap;
	//object of solution manager
	private static SolutionManager instance = null;
	//file name

	private static final String FILE_NAME = "resources/solution.dat";
	
	
	/**
	 * 	protected SolutionManager() 
	 * c'tor
	 */
	@SuppressWarnings("unchecked")
	protected SolutionManager() {	
		solutionsMap = new HashMap<String, Solution>();
			try {
			FileInputStream input = new FileInputStream(FILE_NAME);
			 if(input.available()>0)
			    {
			    ObjectInputStream in=new ObjectInputStream(input);
			    solutionsMap=(HashMap <String, Solution>) (in.readObject());
			    input.close();
			    in.close();
			    }
	        }catch (FileNotFoundException  e) {
	        	// TODO Auto-generated catch block
				e.printStackTrace();
				return;
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	/**
	 * public static SolutionManager getInstance()
	 *method that create solution manager object only once
	 */
	public static SolutionManager getInstance() {
		if (instance == null) {
			instance = new SolutionManager();			
		}
		return instance;
	}
	/**
	 * 	public void addSolution(Solution solution)
	 * method that add a solution for the hash map
	 */
	public void addSolution(Solution solution) {
		solutionsMap.put(solution.getProblemDescription(), solution);
	}
	/**
	 * 	public Solution getSolution(String problemDescription)
	 * method that get solution from the hash map
	 */
	public Solution getSolution(String problemDescription) {
		return solutionsMap.get(problemDescription);
	}
	/**
	 * 	public void saveSolutionsInFile()
	 * method that write solution to file
	 */
	public void saveSolutionsInFile() {
		
		FileOutputStream out = null;
		ObjectOutputStream oos = null;
		try {
			out = new FileOutputStream(FILE_NAME);
			oos = new ObjectOutputStream(out);
			oos.writeObject(solutionsMap);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
       } finally {
	    if (out != null) {
	     try {
	    out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	  }
    }
  }
	
	
	
}






