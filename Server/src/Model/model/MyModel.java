package Model.model;

import java.util.ArrayList;
import java.util.Observable;

import Model.Algorithm.Action;
import Model.Algorithm.Heuristic;
import Model.Algorithm.HeuristicSearcher;
import Model.Algorithm.SearchDomain;
import Model.Algorithm.Searcher;



/**
 * public class MyModel extends Observable implements Model 
 * class my model 
 */
public class MyModel extends Observable implements Model {
	// data members

	// type of search domain
	private SearchDomain domain;
	

	// type of algorithm
	private Searcher algorithm;
	// algorithm factory
	private SearchAlgorithmsFactory algorithmsFactory;
	// domain factory
	private SearchDomainFactory domainFactory;
	// solution
	private Solution solution;
	// domain name
	private String DomainName;
	//manage all solution
	private SolutionManager solutionManager;
	//model thread
	protected Thread ModelThread;
	
	
	/**
	 * public MyModel()
	 * c'tor
	 */
	public MyModel() {
		// initialize all data member
		this.domain = null;
		this.algorithm = null;
		this.solution = null;
		this.DomainName = "";
		this.algorithmsFactory = new SearchAlgorithmsFactory();
		this.domainFactory = new SearchDomainFactory();
		solutionManager = SolutionManager.getInstance();
		this.ModelThread=null;
	}
	
	/**
	 *public void selectDomain(String domainName)
	 *  method that create the domain according to his name and parameters
	 */
	@Override
	public void selectDomain(String domainName) {
		String[] str = domainName.split(":");
		DomainName = str[0];
		domain = domainFactory.createDomain(domainName);
	}
	
	/**
	 * public void selectAlgorithm(String algorithmName) 
	 * method that create the algorithm according to his name
	 */
	@Override
	public void selectAlgorithm(String algorithmName) {
		algorithm = algorithmsFactory.createAlgorithm(algorithmName);
		if (algorithm instanceof HeuristicSearcher) {
			Heuristic h = new HeuristicFactory().createHeuristic(DomainName);
			((HeuristicSearcher) algorithm).SetH(h);
		}
	}
	/**
	 * public void solveDomain() 
	 * method that solve the problem in the domain with the search algorithm chosen
	 */
	@Override
	public void solveDomain() {
		String problemDescription = domain.getProblemDescription();
		Solution sol = solutionManager.getSolution(problemDescription);
		if (sol == null) {//check if not exist solution
			ArrayList<Action> actions = algorithm.search(domain);
			solution = new Solution();
			solution.setProblemDescription(domain.getProblemDescription());
			solution.setActions(actions);
		}

		else //exist solution
		{
			solution = sol;}
		
	}
	/**
	 * public Solution getSolution()
	 * returns the solution found
	 */
	@Override
	public Solution getSolution() {
		return solution;
	}
	
	public SearchDomain getDomain() {
		return domain;
	}

	public void setDomain(SearchDomain domain) {
		this.domain = domain;
	}
	/**
	 * public String getNameDomain() 
	 * Returns the domain name
	 */
	public String getNameDomain() {
		return this.DomainName;
	}
	/**
	 * public void doTask() 
	 * solve domain
	 */
	@Override
	public void doTask() {
		solveDomain();
	}
	
	/**
	 * public void stopModel()
	 * method that stop the thread of the model
	 */
	@Override
	public void stopModel() {
		algorithm.setStop();
	}


}
