package Model.model;

import java.util.HashMap;
import Model.Algorithm.Heuristic;
import Model.Domains.MazeGameHeuristic;
import Model.Domains.PuzzleGameHeuristic;


/**
 *public class HeuristicFactory 
 *heuristic factory
 */
public class HeuristicFactory 
{
	//data member
	//HashMap that saves for each domain his heuristic creator
	private HashMap<String, HeuristicCreator> heuristic;
	
	/**
	 * public HeuristicFactory()
	 *c'tor
	 */
	public HeuristicFactory()
	{
		//initialize the hash map
		 heuristic = new HashMap <String, HeuristicCreator> ();
		 heuristic.put("Maze", new MazeHeuristicCreator());
		 heuristic.put("Puzzle", new PuzzleHeuristicCreator());
	}
	
	/**
	 * public Heuristic createHeuristic(String arg)
	 *create the heuristic function according to the hash map: MazeHeuristicCreator or PuzzleHeuristicCreator
	 */
	public Heuristic createHeuristic(String arg)
	{
		HeuristicCreator creator = heuristic.get(arg);
		Heuristic searcher = null;
		if (creator != null)  {
			searcher = creator.create();			
		}
		return searcher;
	}
	/**
	 * private interface HeuristicCreator
	 *interface for all type of heuristic creator
	 */
	private interface HeuristicCreator
	{
		Heuristic create();
	}
	/**
	 * private class MazeHeuristicCreator implements HeuristicCreator
	 *class that create the maze heuristic function
	 */
	private class MazeHeuristicCreator implements HeuristicCreator
	{
		@Override
		public Heuristic create() {
			// TODO Auto-generated method stub
			return new MazeGameHeuristic();
		}		
	}
	/**
	 * 	private class PuzzleHeuristicCreator implements HeuristicCreator
	 *class that create the puzzle heuristic function
	 */
	private class PuzzleHeuristicCreator implements HeuristicCreator
	{
		@Override
		public Heuristic create() {
			// TODO Auto-generated method stub
			return new PuzzleGameHeuristic();
		}		
	}
}



