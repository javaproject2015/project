package Main;

import java.beans.XMLEncoder;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import Settings.ServerProperties;


/**
 * public class Main
 *create xml file for the first time
 */
public class Main {
	private static final String FILE_NAME = "resources/Properties.xml";

	public static void main(String[] args) {
		ServerProperties properties = new ServerProperties(4862,5);
		
		XMLEncoder encoder = null;
		try {
			encoder = new XMLEncoder(new FileOutputStream(FILE_NAME));
			encoder.writeObject(properties);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			encoder.close();
		}		
	}
}
