package Settings;

import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * public class HandleProperties
 * read client from XML file
 */
public class HandleProperties {
	
private static final String FILE_NAME = "resources/Properties.xml";

   /**
	* public static ClientProperties readProperties()
	* read object from XML file
   */
	public static ClientProperties readProperties() {
		XMLDecoder decoder = null;
		
		try {
			decoder = new XMLDecoder(new FileInputStream(FILE_NAME));
			ClientProperties properties = (ClientProperties)decoder.readObject();
			return properties;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			decoder.close();
		}
		return null;
	}
}
