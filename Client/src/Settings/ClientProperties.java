package Settings;

import java.io.Serializable;


/**
 * public class ClientProperties implements Serializable 
 * Class that contains the client settings 
 */
public class ClientProperties implements Serializable {
	//data members
	private static final long serialVersionUID = 1L;
	//port client connect
	private int port;
	//server address 
	private String address;
	

	/**
	 * 	public ClientProperties(int port, String address)
	 * c'tor
	 */
	public ClientProperties(int port, String address) {
		super();
		this.port = port;
		this.setAddress(address);
	}	

	/**
	 * public ClientProperties()
	 *default c'tor
	 */

	public ClientProperties() { }
	
	 /**
	* public int getPort()
	* get port
    */
	public int getPort() {
		return port;
	}
	 /**
	* public void setPort(int port)
	* set port
	 */
	public void setPort(int port) {
		this.port = port;
	}
	 /**
	* 	public String getAddress() 
	*
	 */
	public String getAddress() {
		return address;
	}
	 /**
	* 	public void setAddress(String address)
	*   set Address 
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	

		
}



