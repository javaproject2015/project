package View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

import Model.Algorithm.Action;
import Model.model.Solution;

public class MazeGameWindow extends UIView{

	private String userAction;
	private List actionsList;
	private Maze maze;	
	private int [][] boardGame;
	
	
	public MazeGameWindow(int width, int height, String title) {
		super(width, height, title);
		
	}

	@Override
	void initWidgets() {
		super.initWidgets();
		shell.setLayout(new GridLayout(2, false));
		Label ChooseLevel = new Label(shell, SWT.NONE);
		ChooseLevel.setText ("Choose Level");
		maze=new Maze(shell, SWT.BORDER);
	    maze.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,true,1,7));
	    maze.redraw();
		final Combo combo1 = new Combo(shell, SWT.READ_ONLY);
		combo1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		String items1[] = {"EAZY","MEDIUM","HARD"};
		combo1.setItems(items1);
		Label lblAlgorithm = new Label(shell, SWT.NONE);
		lblAlgorithm.setText(" Choose algorithm: ");
		final Combo combo2 = new Combo(shell, SWT.READ_ONLY);
		combo2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		String items2[]={ "BFS", "Astar" };
		combo2.setItems(items2);
		Button btnSearch = new Button(shell, SWT.PUSH);
		btnSearch.setText("Search");
		btnSearch.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,false));
		Button btnSolution = new Button(shell, SWT.PUSH);
		btnSolution.setText(" Show Solution ");
		btnSolution.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,false));
		actionsList = new List(shell, SWT.BORDER | SWT.V_SCROLL);
		actionsList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		
		btnSearch.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
	
				switch (combo1.getText()) {
				case "EAZY": userAction = "SelectDomain Maze:"+5+","+5+","+2;
							break;
				case "MEDIUM": userAction = "SelectDomain Maze:"+25+","+25+","+12;
							break;
				case "HARD": userAction = "SelectDomain Maze:"+125+","+125+","+62;
							break;
				default: userAction = "SelectDomain Maze:"+25+","+25+","+12;
					break;
				}
				
				  MazeGameWindow.this.setChanged();
				  MazeGameWindow.this.notifyObservers();
				  System.out.println("after select domain "+userAction);
				
				  userAction = "SelectAlgorithm " + combo2.getText();
				  MazeGameWindow.this.setChanged();
				  MazeGameWindow.this.notifyObservers();
				  System.out.println("after select algorithm "+userAction);
				  
				  userAction = "SolveDomain";
				  MazeGameWindow.this.setChanged();
				  MazeGameWindow.this.notifyObservers();
				  System.out.println("after solve "+userAction);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {	
			}
		});
		
		btnSolution.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				userAction = "haveAsolution? 1";
				MazeGameWindow.this.setChanged();
				MazeGameWindow.this.notifyObservers();
				System.out.println("after have a solution "+userAction);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});	 
	}

	
	@Override
	public void start() {
		run();
	}

	@Override
	public void displayCurrentState() {
	}

	@Override
	public void displaySolution(Solution solution) {
	//	maze.start(arg1);
		if(solution.getActions()==null)
		{
			System.out.println("no solution");
		}else{
			for (Action a : solution.getActions()) {
				display.asyncExec(new Runnable() {
					@Override
					public void run() {
						actionsList.add(a.toString());
						}
					});
				}
			}
		maze.start(boardGame,solution);
		}

	@Override
	public String getUserAction() {
		return userAction;
	}

	
	@Override
	public void displayMessage(String str) {
	}

	@Override
	public void paintBoardGame(final int[][] arg1) {
		if(arg1!=null){
			this.boardGame=arg1;
			maze.setBoard(arg1);
			maze.redraw();
			}
		
	}

}