package View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public abstract class UIView extends BasicWindow {
	
	public UIView(int width, int height, String title) {
		super(width, height, title);
		// TODO Auto-generated constructor stub
	}

	 	@Override
		void initWidgets() {
		Menu menu = new Menu(shell, SWT.BAR);
		Menu fileMenu = new Menu(menu);
		MenuItem fileItem = new MenuItem(menu, SWT.CASCADE);
		fileItem.setText("File");
		fileItem.setMenu(fileMenu);
		Menu fileMenu2 = new Menu(menu);
		MenuItem fileItem2 = new MenuItem(menu, SWT.CASCADE);
		fileItem2.setText("help");
		fileItem2.setMenu(fileMenu2);
		shell.setMenuBar(menu);
		MenuItem aboutItem2 = new MenuItem(fileMenu2, SWT.NONE);
		aboutItem2.setText("About");
		MenuItem openItem = new MenuItem(fileMenu, SWT.NONE);
		openItem.setText("Open");
		MenuItem exitItem = new MenuItem(fileMenu, SWT.NONE);
		exitItem.setText("Exit");
		aboutItem2.addSelectionListener(new SelectionListener(){
		
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		openItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				openConfig(shell);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		exitItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				promptExit();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
	
		
		// Display the main shell when this shell is closed
		shell.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {
				// TODO Auto-generated method stub
				Shell mainShell = shell.getDisplay().getShells()[0];
				mainShell.setVisible(true);
			}
		});
	}
		
		@Override
		public void run() {
			super.run();
		}
		
		public void promptExit() {
			
			
			final MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING| SWT.YES | SWT.NO);
			messageBox.setMessage("Do you really want to exit?");
			messageBox.setText("Exiting Application");
			int response = messageBox.open();
			if (response == SWT.YES)
			closeUI();
		}

		// //////////////////////////////////// ���� ������ ��� ����� �� �������
		// ////////
		public void closeUI() {

			if (!shell.isDisposed()) {
				shell.dispose();
			}
		}
		
		private void openConfig(final Shell shell) {
			/* this method calls for the open dialog, and gets the path to the XML
			 * file. then, it updates the necessary parts */
			FileDialog fd = new FileDialog(shell, SWT.OPEN);
			fd.setText("Choose Configuration File");
			fd.setFilterPath("C:/Users/roi/Project");
			String[] filterExt = "*.xml,*.*".split(",");
			fd.setFilterExtensions(filterExt);
			String path = fd.open();

			if (path != null) {
				// tell the controller to set the config file of the model
			/*	controller.setModelConfig(path);*/
			}
		
		}

		public abstract void paintBoardGame(int[][] arg1);
		
	}
	
	
	
	

