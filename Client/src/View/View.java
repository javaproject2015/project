package View;

import Model.model.Solution;



//interface view
public interface View 
{
	//method that start the game
	void start();
	// display the current state
	void displayCurrentState();
	//display the solution of the problem
	void displaySolution(Solution solution);
	// return the action
	String getUserAction();
	//method that show message to the user
	void displayMessage(String str);
	
}
