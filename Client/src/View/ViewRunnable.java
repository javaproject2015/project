package View;

//class that open thread when user insert a problem

public class ViewRunnable implements Runnable {

	//data members
	View view;
	
	//c'tor
	public ViewRunnable (View view)
	{
		this.view=view;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		this.view.start();
	}

}
