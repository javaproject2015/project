package View;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;

class GameCharacter {

	int x, y;

	public GameCharacter(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void paint(PaintEvent e, int w, int h) {
		e.gc.setForeground(new Color(null, 175, 0, 50));
		e.gc.drawRectangle(x, y, w, h);
	}
}
