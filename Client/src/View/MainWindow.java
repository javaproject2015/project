package View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;

import Model.model.Solution;



public class MainWindow extends BasicWindow {

	public MainWindow(int width, int height,String title){
		super(width, height, title);
		display.loadFont("resources/font.ttf");
	}

	@Override
	void initWidgets() {
		/* Menu bar */
		Menu menu = new Menu(shell, SWT.BAR);
		Menu fileMenu = new Menu(menu);
		MenuItem fileItem = new MenuItem(menu, SWT.CASCADE);
		fileItem.setText("File");
		fileItem.setMenu(fileMenu);
		shell.setMenuBar(menu);

		MenuItem exitItem = new MenuItem(fileMenu, SWT.NONE);
		exitItem.setText("Exit");

		exitItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				promptExit();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		shell.setLayout(new GridLayout(1, false));
		GridLayout gridLayout =new GridLayout();
		gridLayout.numColumns = 2;
		Font font = new Font(display,"DS Dots",45,SWT.NORMAL);
		shell.setLayout(gridLayout);
		Label title = new Label(shell,SWT.CENTER);
		title.setText("\nCHOOSE GAME");
		title.setFont(font);
		GridData gridData = new GridData(GridData.FILL,GridData.CENTER,true,false);
		gridData.horizontalSpan = 2;
		title.setLayoutData(gridData);
		
		Image image1 = new Image(display, "resources/maze.jpg");
		Button button1 = new Button(shell, SWT.PUSH);
		button1.setImage(image1);
		button1.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, true));
		button1.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// Choose window according to the game (using a factory)
					display.dispose();
					UIView window = new MazeGameWindow(900, 600, "Maze Game");
					MainWindow.this.setChanged();
					MainWindow.this.notifyObservers(window);
					window.run();					
					//shell.setVisible(false);
				}
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
						
				}		
		});

		Image image2 = new Image(display, "resources/puzzle.jpg");
		Button button2 = new Button(shell, SWT.PUSH);
		button2.setImage(image2);
		button2.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, true));
		button2.addSelectionListener(new SelectionListener() {
					
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// Choose window according to the game (using a factory)
						display.dispose();
						UIView window = new PuzzleGameWindow(900, 600, "Puzzle Game");
						MainWindow.this.setChanged();
						MainWindow.this.notifyObservers(window);
						window.run();					
						//shell.setVisible(false);
						
					}

				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
				}
					
		});
	}
	
	
	public void promptExit() {
		final MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING
				| SWT.YES | SWT.NO);
		messageBox.setMessage("Are you sure you want to EXIT?");
		display.asyncExec(new Runnable() {

			@Override
			public void run() {
				int rc = messageBox.open();
				switch (rc) {
				case SWT.YES:
					closeUI();
					break;
				}
			}
		});
	}

	// //////////////////////////////////// ���� ������ ��� ����� �� �������
	// ////////

	public void closeUI() {

		if (!shell.isDisposed()) {
			shell.dispose();
		}

	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayCurrentState() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displaySolution(Solution solution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getUserAction() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void displayMessage(String str) {
		// TODO Auto-generated method stub
		
	}

}
