package View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Observable;

import Model.Algorithm.Action;
import Model.model.Solution;

//class that implements view
public class MyConsoleView extends Observable implements View {

	//data member
	//the action choose
	private String action;
	
	//method that start the game
	@Override
	public void start() 
	{
		//explain the user how to write his data
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Welcome to my project!");
		System.out.println("for select a game please enter SelectDomain and then Maze: or Puzzle");
		System.out.println("if you choose Maze please don't forget to enter number of rows,column and walls");
		System.out.println("for example:SelectDomain Maze:5,5,2  means board 5x5 with 2 walls" );
		System.out.println("for select a algorithm type please enter SelectAlgorithm and then BFS or Astar");
		System.out.println("if you want to solve the game please enter SolveDomain");
		System.out.println("if you want show the solution please enter this string haveAsolution?  with the munber of game you want ");
		System.out.println("for example:haveAsolution? 1" );
		System.out.println("when you finish please enter exit" );

		action="";
		System.out.print("Enter command: ");
		//getting actions from the user until the user write exit
		try {
			while(!((action=in.readLine()).equals("exit"))){
				this.setChanged();
				this.notifyObservers();
				System.out.print("Enter command: ");
			}
		} catch (IOException e) {
			System.out.println("worng command");
			e.printStackTrace();
		}
		this.setChanged();
		this.notifyObservers();
	}
	
	// display the current state
	@Override
	public void displayCurrentState () 
	{
	}

	//display the solution of the problem
	@Override
	public void displaySolution(Solution solution) {
		
		if(solution.getActions()==null)
		{
			System.out.println("no solution");
		}
		else
		{
		for(Action a : solution.getActions())
			System.out.println(a);
		}
	}

	// return the action
	@Override
	public String getUserAction() {		
		return action;
	}


	//show the message get from presenter
	@Override
	public void displayMessage(String str) {
		System.out.println(str);
		
	}

	



}

