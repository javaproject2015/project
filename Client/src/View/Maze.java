package View;

import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import Model.model.Solution;


public class Maze extends Canvas {

	// data member

	GameCharacter character;
	private HashMap<String, Command> moves;
	Timer timer;
	TimerTask task;
	int[][] mazeData;

	public Maze(Composite parent, int style) {
		super(parent, style);
		setBackground(new Color(null, 255, 255, 255));
		this.mazeData = null;
		this.moves = new HashMap<String, Command>();
		moves.put("rigth", new MoveRigthCommand());
		moves.put("left", new MoveLeftCommand());
		moves.put("up", new MoveUpCommand());
		moves.put("down", new MoveDownCommand());
		this.character = new GameCharacter(0,0);
		this.addPaintListener(new PaintListener() {

			@Override
			public void paintControl(PaintEvent e) {
			if(mazeData!=null){
				e.gc.setForeground(new Color(null, 250, 175, 250));
				e.gc.setBackground(new Color(null, 250, 175, 250));
				int width =getSize().x;
				int height = getSize().y;
				int w = width / mazeData[0].length;
				int h = height / mazeData.length;

				for (int i = 0; i < mazeData.length; i++)
					for (int j = 0; j < mazeData[i].length; j++) {
						int x = j * w;
						int y = i * h;
						if (mazeData[i][j] != 0)
							e.gc.fillRectangle(x, y, w, h);
					}
				character.paint(e, w, h);
	
			}
			}

		});
		redraw();
		
	
	}
	public void start(int[][] maze, Solution solution) {
		this.timer = new Timer();
		this.task = new TimerTask() {
			@Override
			public void run() {
				getDisplay().syncExec(new Runnable() {
					@Override
					public void run() {
						for (int i=0;i<solution.getActions().size();i++) {
							System.out.println("in for");
							Command command = moves.get((solution.getActions()).get(i));
							if (command != null)
								command.doCommand();
						}
						redraw();
					}
				});
			}
		};
	}

	public void stop() {
		if (task != null)
			task.cancel();
		if (timer != null)
			timer.cancel();
	}

	public void setBoard(int[][] arg1) {
		mazeData=arg1;
	}
	
	public GameCharacter getCharacter() {
		return character;
	}

	public void setCharacter(GameCharacter character) {
		this.character = character;
	}
	
	public interface Command
	{
		void doCommand();
	}

	private class MoveRigthCommand implements Command {

		@Override
		public void doCommand() {
			character.x+= getSize().x/mazeData.length;
			redraw();
			update();
		}
	}

	private class MoveLeftCommand implements Command {

		@Override
		public void doCommand() {
			character.x -= getSize().x/mazeData.length;;
			redraw();
			update();

		}

	}

	private class MoveUpCommand implements Command {

		@Override
		public void doCommand() {
			character.y += getSize().y/mazeData[0].length;;
			redraw();
			update();

		}

	}

	private class MoveDownCommand implements Command {

		@Override
		public void doCommand() {
			character.y -= getSize().y/mazeData[0].length;;
			redraw();
			update();

		}

	}

	

}
