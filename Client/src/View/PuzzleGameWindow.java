package View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import Model.Algorithm.Action;
import Model.model.Solution;

public class PuzzleGameWindow extends UIView  {

	private String userAction;
	private List actionsList;
	
	
	
	
	/////////////////////////
	private Puzzle puzzle;
	/////////////////
	
	
	public PuzzleGameWindow(int width, int height, String title) {
		super(width, height, title);
		// TODO Auto-generated constructor stub
		userAction="";
		actionsList=null;
	}

	@Override
	void initWidgets() {
		super.initWidgets();
		shell.setLayout(new GridLayout(2, false));
		puzzle=new Puzzle(shell, SWT.BORDER);
		puzzle.redraw();
		Group input = new Group(shell, SWT.SHADOW_OUT);
		input.setText("Input");
		input.setLayout(new GridLayout(8, true));
		Label lblStartWord = new Label(input,SWT.NONE);
		lblStartWord.setText("   Tile order:    ");  
		final Text txtPuzzleStart = new Text(input, SWT.BORDER);
	
		Label lblAlgorithm = new Label(input, SWT.NONE);
		lblAlgorithm.setText("Choose algorithm: ");
		final Combo combo = new Combo(input, SWT.READ_ONLY);
		combo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
		String items[] = { "BFS", "A-Star" };
		combo.setItems(items);
		Button shuffle = new Button(input, SWT.PUSH);
		shuffle.setText("         Shuffle          ");
		shuffle.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,false, 2, 1));
		Button solve = new Button(input, SWT.PUSH);
		solve.setText("         solve          ");
		solve.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,false, 2, 1));
		actionsList = new List(shell, SWT.BORDER | SWT.V_SCROLL);
		actionsList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,2, 1));
		shuffle.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
		
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {	
			}
		});  
	}

	@Override
	public void start() {
		run();
	}

	@Override
	public void displayCurrentState() {
		// TODO Auto-generated method stub
	}

	@Override
	public void displaySolution(Solution solution) {
		for (Action a : solution.getActions()) {
			display.asyncExec(new Runnable() {
				@Override
				public void run() {
					actionsList.add(a.toString());
				}
			});
		}
	}

	@Override
	public String getUserAction() {
		return userAction;
	}
	
	@Override
	public void displayMessage(String str) {
		// TODO Auto-generated method stub
	}

	@Override
	public void paintBoardGame(int[][] arg1) {
		// TODO Auto-generated method stub
		
	}


}