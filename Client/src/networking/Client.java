package networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import Model.model.Problem;
import Model.model.Solution;
import Settings.ClientProperties;
import Settings.HandleProperties;

/**
 * public class Client client class
 */
public class Client {
	// data members
	// server address
	String serverAddress;
	// port to connect
	int port;
	// client socket
	Socket socket;
	// read from server
	ObjectInputStream in;
	// write to server
	ObjectOutputStream out;

	/**
	 * public Client() c'tor
	 */
	public Client() {
		ClientProperties properties = HandleProperties.readProperties();
		this.serverAddress = properties.getAddress();
		this.port = properties.getPort();
		try {
			this.socket = new Socket(serverAddress, port);
			this.out = null;
			this.in = null;
		} catch (IOException e) {
			System.out.println("can't connected to server");
		}
	}

	/**
	 * public void setSolution(Problem problem) { notifies server that the
	 * client wants a solution
	 */
	public String solveDomain(Problem problem) {
		// Solution solution=null;
		String boardGame = null;
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			if (socket != null) {
				out.writeObject("SolveDomain");
				out.writeObject(problem);
				try {
					boardGame = (String) in.readObject();
					// solution=(Solution)in.readObject();
					return boardGame;
				} catch (IOException e) {
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * finally{ exitClient(); }
		 */
		return boardGame;
	}

	public Solution ShowSolution() {
		Solution solution = null;
		try {
			if (socket != null) {
				out.writeObject("ShowSolution");
				try {
					solution = (Solution) in.readObject();
					return solution;
				} catch (IOException e) {
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		finally {
			exitClient();
		}
		return solution;
	}

	/**
	 * public void exitClient() notifies server that the client wants to
	 * disconnect
	 */
	public void exitClient() {
		try {
			if (out != null)
				out.close();
			if (in != null)
				in.close();
			if (socket != null)
				socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
