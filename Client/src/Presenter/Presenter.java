package Presenter;

import java.util.Observable;
import java.util.Observer;

import Model.model.Model;
import Model.model.ModelManager;
import Model.model.MyModel;
import Presenter.UserCommands.Command;
import View.MainWindow;
import View.UIView;
import View.View;

/**
 * public class Presenter implements Observer class that linking between the
 * view and the model
 */
public class Presenter implements Observer {
	// data members
	// model
	private Model model;
	// view
	private View view;
	// command
	private UserCommands commands;
	protected static Thread t;
	// client's models
	private ModelManager models;

	public Presenter(Model model) {
		this(model, null);
	}

	// c'tor
	public Presenter(Model model, View view) {
		this.model = model;
		this.view = view;
		this.models = new ModelManager();
		commands = new UserCommands();
	}

	/**
	 * public void update(Observable observable, Object arg1) method that update
	 * data when something happen in the model or in the view
	 */
	@Override
	public void update(Observable observable, Object arg1) {
		String action;
		String message = "";
		if (observable instanceof Model) {
			if (arg1 != null) {
				if (arg1 instanceof int[][]) {
					((UIView)view).paintBoardGame((int[][])arg1);
				} else {
					int index = ((Integer) arg1).intValue();
					message = findSolution(index);
					if (message.equals("soultion is ready")) {
						view.displaySolution(models.FindModel(index).getSolution());
					}
				}
			} else {
				view.displayMessage(message);
			}

		} else if (observable instanceof MainWindow) {
			if (arg1 instanceof UIView) {
				view = (View) arg1;
				((Observable) arg1).addObserver(this);
			}
		} else {
			if (arg1 == null) {
				action = view.getUserAction();
				String[] arr = action.split(" ");
				String commandName = arr[0];
				String args = null;
				if (arr.length > 1)
					args = arr[1];
				Command command = commands.selectCommand(commandName);
				Model m = command.doCommand(model, args);
				if (m != null) {
					// Check if we got a new model from the command
					if (m != model) {
						this.model = m;
						models.SetModel(m);// set model in the models array
						m.addObserver(this);
					}
				}
			}
		}
	}

	public String findSolution(int index) {
		String message = "";
		Model m = models.FindModel(index);// find the model by index
		if (m != null) {
			message = m.checkThread(index);// check solution status
			return message;// return message according check
		} else {
			message = "invaild input";
			return message;
		}
	}

	/**
	 * public static void main(String[] args) main
	 */
	public static void main(String[] args) {
		// create model
		MyModel model = new MyModel();
		// create view
		// MyConsoleView view = new MyConsoleView();
		// create presenter
		// Presenter presenter =new Presenter(model, view);
		Presenter presenter = new Presenter(model);
		MainWindow view = new MainWindow(900, 600, "Select game");
		// add the presenter as observer of the model
		model.addObserver(presenter);
		// add the presenter as observer of the view
		view.addObserver(presenter);
		// start the game
		// t = new Thread(new ViewRunnable(view));
		// t.start();
		view.run();
	}

	public void setView(View view) {
		this.view = view;

	}

}
