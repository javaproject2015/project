package Presenter;

import java.util.HashMap;

import Model.model.Model;
import Model.model.MyModel;


/**
 * public class UserCommands
 * class that deal with the input and classifies it
 */
public class UserCommands {
	
	//data member
	// HashMap that save for each command it's type
	private HashMap<String, Command> commands = new HashMap<String,Command>();
	

	/**
	 *public UserCommands()
	 * c'tor
	 */
	public UserCommands()
	{
		//initialize the HashMap
		commands.put("SelectDomain", new SelectDomainCommand());
		commands.put("SelectDetails", new SelectDomainCommand());
		commands.put("SelectAlgorithm", new SelectAlgorithmCommand());
		commands.put("SolveDomain", new SolveDomain());
		commands.put("haveAsolution?", new HaveASolution());
		commands.put("exit", new exitCommand());
	}
	  /**
		 * public void doCommand(Model model, String commandName, String args)                                 
		 * method that do what in the command according to the HashMap
		 */
	public void doCommand(Model model, String commandName, String args)
	{
		Command command = commands.get(commandName);
		if (command != null)
			command.doCommand(model, args);
	}
	 /**
	 * public interface Command
	 * interface of command
	 */
	public interface Command
	{
		Model doCommand(Model model, String args);
	}
	 /**
	 * public Command selectCommand(String commandName)
	 * method that return command 
	 */
	public Command selectCommand(String commandName)
	{
		Command command = commands.get(commandName);
		return command;
	}
	 /**
	* private class SelectDomainCommand implements Command
	* class that select the domain
	*/
	
	private class SelectDomainCommand implements Command
	{
		
		//	Model m;
			@Override
			public Model doCommand(Model model, String args) {
				Model m = new MyModel();
				m.selectDomain(args);	
				return m;
			}
			/*if(args!=null){
				if(args.equals("Maze:")|| args.equals("Puzzle")){
					m = new MyModel();
					m.selectDomain(args);	
					return m;}
				else{
					if(args!=null)
						m.selectDetails(args);	
						return m;
				}
			}
			return m;
			}*/
		
	}

	
	    /**
		* private class SelectAlgorithmCommand implements Command
		* class that select the algorithm
		*/
	private class SelectAlgorithmCommand implements Command
	{
		@Override
		public Model doCommand(Model model, String args) {
			model.selectAlgorithm(args);
			return model;
		}		
	}
    /**
		* private class SolveDomain implements Command
		* class that solve the problem- solve domain in model thread
		*/
	private class SolveDomain implements Command
	{
		@Override
		public Model doCommand(Model model, String args) {
			model.solveDomain();
			return model;
		}
				
	}
    /**
		* private class HaveASolution implements Command
		* class that check if have a solution for specific problem the user insert
		*/
	private class HaveASolution implements Command
	{

		@Override
		public Model doCommand(Model model, String args) {
			int index=Integer.parseInt(args);
			model.haveAsolution(index-1);
			return model;
		}
	}
	/**
	* private class exitCommand implements Command
	* class that makes an orderly exit of all threads
	*/
	private class exitCommand implements Command
	{
		@Override
		public Model doCommand(Model model, String args) {
			
			model.exit();
			return model;
		}
	}
	
	
}
	
	

