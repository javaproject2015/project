package Model.Algorithm;

import java.io.Serializable;

/**
 * public class Action implements Serializable
 *class that describes a general action for a game
 */
@SuppressWarnings("serial")
public class Action implements Serializable {
	
	//data members
	
	//description of the action
	String description;
	//action's price
	private double price;
	
	/**
	 * public Action(String description)
	 *c'tor
	 */
	public Action(String description) {
		this.description=description;
		this.price=0;
	}
	/**
	 * public int hashCode()
	 *a hash code value for Action object
	 */
	@Override
	public int hashCode(){
		return description.hashCode();
	}
	/**
	 *public String toString()
	 *return a string representation of the Action
	 */
	@Override
	public String toString(){
		return description;
	}
	/**
	 * public double getPrice() 
	 *return the price of the Action
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * 	public void setPrice(double price)
	 *  set the price of the Action
	 */
	public void setPrice(double price) {
		this.price = price;
	}
}


