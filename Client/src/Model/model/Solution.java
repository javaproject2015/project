package Model.model;

import java.io.Serializable;
import java.util.ArrayList;

import Model.Algorithm.Action;


/**
 *public class Solution implements Serializable
 *class that show the solution for the domain problem
 */
@SuppressWarnings("serial")
public class Solution implements Serializable {
	
	//data members
	//list of action to solve the domain's problem
	private ArrayList<Action> actions;
	//problem description 
	private String problemDescription;
	
	
	/**
	 * public ArrayList<Action> getActions()
	 * actions get
	 */
	public ArrayList<Action> getActions() 
	{
		return actions;
	}
	/**
	 * public ArrayList<Action> getActions()
	 * actions set
	 */
	public void setActions(ArrayList<Action> actions) 
	{
		this.actions = actions;
	}
	/**
	 * public String getProblemDescription()
	 * actions get
	 */
	public String getProblemDescription() {
		return problemDescription;
	}
	/**
	 * public void setProblemDescription(String problemDescription)
	 *  set Problem Description
	 */
	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}
}
	
