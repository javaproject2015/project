package Model.model;

import java.util.Observable;

import Tasks.TaskRunnable;
import networking.Client;


/**
 * public class MyModel extends Observable implements Model
 *class my model 
 */
public class MyModel extends Observable implements Model {
	// data members
	// client problem
	private Problem problem;
	//client solution
	private Solution solution;
	private int[][] boardGame;
	private Thread ModelThread;
	//client
	private Client client;
	/**
	 * public MyModel()
	 * c'tor
	 */
	public MyModel() {
		problem = new Problem();
		solution= new Solution();
		ModelThread=null;
	}
	/**
	 * public void selectDomain(String args)
	 * method that save domain details in problem
	 */
	@Override
	public void selectDomain(String args) {
		problem.setDomainDetails(args);
		
	}
	/**
	 * public void selectDetails(String args)
	 * method that save details in domain
	 */
	public void selectDetails(String args) {
		problem.setDomainDetails(args);
		
	}
	/**
	 * public void selectAlgorithm(String algorithmName)
	 * method that save algorithm details in problem
	 */
	@Override
	public void selectAlgorithm(String algorithmName) {
		problem.setAlgorithmName(algorithmName);
	}
	/**
	 * public void solveDomain() 
	 * through client notifies the server on solve domain request 
	 */
	@Override
	public void solveDomain() {
		this.client=new Client();
		String boardGame=client.solveDomain(problem);
		this.boardGame=convertToArray(boardGame);
		this.setChanged();
		this.notifyObservers(this.boardGame);
		
		Thread t = new Thread(new TaskRunnable(this));//create thread for the solution
		setModelThread(t);
		t.start();
		
	}
	
	
	public int[][] convertToArray(String board){
		String[] arr = board.split(" ");
		int row=Integer.parseInt(arr[0]);
		int column=Integer.parseInt(arr[1]);
		int [][]matrix=new int [row][column];
		int k=2;
		int i=0;
		int j=0;
		for(i=0; i<row;i++)
			for(j=0; j<column; j++)
			{
				matrix[i][j]=Integer.parseInt(arr[k]);
				k++;
			}
		return matrix;
	}
	/**
	 * public void solveDomain() 
	 * 	//through client notifies the server on solution request
	 */
	public void haveAsolution(int index)
	{
		this.setChanged();
		this.notifyObservers(index);
	}
	
	
	public String checkThread(int index) {
		if (!(ModelThread.isAlive())) {
			if (solution != null)
				return "soultion is ready";
			else
				return "no solution";
		}
		return "no solution yet";
	}

	
	/**
	 * 	public Solution getSolution() 
	 * 	set
	 */
	@Override
	public Solution getSolution() {
		return solution;
	}
	/**
	 * 	public void setSolution(Solution solution)
	 * 	set
	 */
	@Override
	public void setModelThread(Thread thread) {
		this.ModelThread=thread;
		
	}
	public Thread getModelThread() {
		return ModelThread;
	}
	/**
	 * 	public void setSolution(Solution solution)
	 * 	set
	 */
	@Override
	public void setSolution(Solution solution) {
		this.solution=solution;
		
	}
	/**
	 * public void doTask()
	 * 	solve domain in thread
	 */
	@Override
	public void doTask() {
		Solution solution=client.ShowSolution();
		if(solution != null)
			this.solution=solution;
	}
	/**
	 * public void doTask()
	 * through client notifies the server on exit request
	 */
	public void exit ()
	{
		client.exitClient();
	}
}

	

