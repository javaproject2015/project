package Model.model;

import java.util.Observer;

import Tasks.Task;

/**
 * public interface Model extends Task 
 *interface model that include methods all models need
 */
public interface Model extends Task 
{
	/**
	 * void selectDomain(String domainName);
	 *select domain
	 */
	void selectDomain(String domainName);
	/**
	 * void selectDetails(String domainDetails)
	 *select Details
	 */

	void selectDetails(String domainDetails);
	/**
	 * void selectAlgorithm(String algorithmName)
	 * select algorithm
	 */
	void selectAlgorithm(String algorithmName);
	/**
	 * void solveDomain()
	 * solve the game
	 */
	void solveDomain();
	/**
	 * Solution getSolution()
	 * get the solution
	 */
	Solution getSolution();
	public void setModelThread(Thread thread);
	public Thread getModelThread();
	/**
	 * void setSolution(Solution solution)
	 * method that add observer
	 */
	void setSolution(Solution solution);
	/**
	 * void addObserver(Observer o)
	 * method that add observer
	 */
	public String checkThread(int index);
	void addObserver(Observer o);
	/**
	 * public void haveAsolution(int index)
	 * through client notifies the server on solution request
	 */
	public void haveAsolution(int index);
	/**
	 * public void exit ()
	 * through client notifies the server on exit request
	 */
	public void exit ();
	
}
