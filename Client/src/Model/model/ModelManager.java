package Model.model;

import java.util.ArrayList;

/**
 * public class ModelManager
 *class that manage all models
 */
public class ModelManager {
	
	//data members
	//ArrayList of model
	private ArrayList<Model> models;
	//model index
	protected int index;

	//c'tor
	public ModelManager() {
		models = new ArrayList<Model>();
	}
	/**
	 *public void SetModel(Model model) 
	 * set model in the array
	 */
	public void SetModel(Model model) {
		if(model!=null){
			if (!models.contains(model))
					models.add(model);
		}
	}
	/**
	 *public ArrayList<Model> getModels()
	 * return array of models
	 */
	public ArrayList<Model> getModels() {
		return models;
	}
	/**
	 *public ArrayList<Model> getModels()
	 * set array of models
	 */
	public void setModels(ArrayList<Model> models) {
		this.models = models;
	}
	/**
	 *public void FreeModels() 
	 * method that stop the thread for each model
	 */
	public void FreeModels() {
		for (int i = 0; i < models.size(); i++)
			if (models.get(i).getModelThread() != null) {
				if (models.get(i).getModelThread().isAlive())
					models.get(i).exit();
			}
		}		

	/**
	 *public Model FindModel(int index)
	 *method that return model according to index get, null if not found
	 */
	public Model FindModel(int index) {
		if(index+1 <= models.size()){
			return models.get(index);}
		else{
			return null;}
		
		
	}

}
