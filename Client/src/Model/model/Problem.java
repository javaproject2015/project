package Model.model;

import java.io.Serializable;


/**
 * public class Problem implements Serializable
 *class that description a problem
 */
public class Problem implements Serializable {
	//data members
	private static final long serialVersionUID = 1L;
	//domain description
	private String domainDetail;
	//algorithm description
	private String algorithmName;
	
	public Problem (){
		this.domainDetail="";
		this.algorithmName="";
	}
	
	/**
	 * 	public String getDomainDetails()
	 * domain description get&set
	 */
	public String getDomainDetails() {
		return domainDetail;
	}
	/**
	 * 	public void setDomainDetails(String str)
	 *    set Domain Details
	 */
	public void setDomainDetails(String str) {
		 this.domainDetail+=str;
		 System.out.println("problem domain "+getDomainDetails());
	}
	/**
	 * 	public void setDomainDetails(String str)
	 *    algorithm description get
	 */
	public String getAlgorithmName() {
		return algorithmName;
	}
	/**
	 * 	public void setDomainDetails(String str)
	 *    algorithm description set
	 */
	public void setAlgorithmName(String str) {
		 this.algorithmName=str;
	}


}
	
